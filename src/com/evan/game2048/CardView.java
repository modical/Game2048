package com.evan.game2048;

import android.content.Context;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.TextView;

public class CardView extends FrameLayout {

	private int num = 0;
	private TextView label;
	
	public CardView(Context context) {
		super(context);
		label = new TextView(context);
		label.setTextSize(32);
		label.setGravity(Gravity.CENTER);
		label.setBackgroundColor(0x33ffffff);
		LayoutParams lp=new LayoutParams(-1,-1);
		lp.setMargins(10, 10, 0, 0);
		addView(label, lp);
		setNum(0);
	}
	
	public int getNum() {
		return num;
	}
	
	public void setNum(int num) {
		this.num = num;
		String textString = "";
		if(num > 0) {
			textString = String.valueOf(num);
		}
		label.setText(textString);
	}

	public boolean equals(CardView o) {
		return this.num == o.num;
	}
}
